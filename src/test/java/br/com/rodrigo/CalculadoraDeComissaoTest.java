
package br.com.rodrigo;

import br.com.rodrigo.CalculadoraDeComissao;
import br.com.rodrigo.Venda;
import static org.junit.Assert.*;
import org.junit.Test;


public class CalculadoraDeComissaoTest {
    
@Test
    public void deveCalcularComissao(){
    
    CalculadoraDeComissao calculadora = new CalculadoraDeComissao();
    
    double resultado = calculadora.calcular(new Venda(25, 8));
    assertEquals(10, resultado, 0.001);
    
    
}
}
